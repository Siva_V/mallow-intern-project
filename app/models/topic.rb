class Topic < ApplicationRecord
  validates :name,length: {minimum: 1}
  validates :price,numericality: { other_than: 0 }
end
